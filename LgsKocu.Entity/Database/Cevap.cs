//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LgsKocu.Entity.Database
{
    using System;
    using System.Collections.Generic;
    
    public partial class Cevap
    {
        public int Id { get; set; }
        public int SoruId { get; set; }
        public int UyeId { get; set; }
        public string Icerik { get; set; }
        public string GorselLink { get; set; }
        public string GorselYol { get; set; }
        public string Durum { get; set; }
    
        public virtual Soru Soru { get; set; }
        public virtual Uye Uye { get; set; }
    }
}
