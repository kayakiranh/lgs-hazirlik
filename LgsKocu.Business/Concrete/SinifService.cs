﻿using System;
using System.Collections.Generic;
using System.Linq;
using LgsKocu.Entity.Database;

namespace LgsKocu.Business.Concrete
{
    public static class SinifService
    {
        private static readonly DatabaseEntities DB = new DatabaseEntities();

        public static List<Sinif> List()
        {
            var result = DB.Sinif.ToList();
            return result;
        }

        public static Sinif Detail(int id)
        {
            if (id == 0) return new Sinif();

            var result = DB.Sinif.Find(id);
            return result;
        }

        public static int Insert(Sinif data)
        {
            DB.Sinif.Add(data);
            DB.SaveChanges();

            return data.Id;
        }

        public static int Update(Sinif model)
        {
            var check = Detail(model.Id);
            check.Ad = model.Ad;
            DB.SaveChanges();

            return model.Id;
        }

        public static bool Delete(int id)
        {
            var data = Detail(id);
            DB.Sinif.Remove(data);
            DB.SaveChanges();

            return Detail(id) == null;
        }
    }
}