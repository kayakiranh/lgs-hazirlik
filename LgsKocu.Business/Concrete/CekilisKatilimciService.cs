﻿using System.Collections.Generic;
using System.Linq;
using LgsKocu.Business.Helper;
using LgsKocu.Entity.Database;

namespace LgsKocu.Business.Concrete
{
    public static class CekilisKatilimciService
    {
        private static readonly DatabaseEntities DB = new DatabaseEntities();

        public static List<CekilisKatilimci> List()
        {
            var result = DB.CekilisKatilimci.ToList();
            return result;
        }

        public static List<CekilisKatilimci> ListByCekilis(int cekilisId)
        {
            var result = DB.CekilisKatilimci.Where(x => x.CekilisId == cekilisId).ToList();
            return result;
        }

        public static CekilisKatilimci Detail(int id)
        {
            if (id == 0) return new CekilisKatilimci();

            var result = DB.CekilisKatilimci.Find(id);
            return result;
        }
        
        public static int Insert(int cekilisId, int uyeId)
        {
            var data = ListByCekilis(cekilisId);
            var check = data.Where(x => x.UyeId == uyeId);
            if (check.Any()) return -1;

            CekilisKatilimci katilimci = new CekilisKatilimci()
            {
                UyeId = uyeId,
                CekilisId = cekilisId
            };

            DB.CekilisKatilimci.Add(katilimci);
            DB.SaveChanges();

            return katilimci.Id;
        }

        public static bool CanJoin(int cekilisId, int uyeId)
        {
            var result = DB.CekilisKatilimci.Where(x => x.UyeId == uyeId && x.CekilisId == cekilisId);
            return !result.Any();
        }

    }
}