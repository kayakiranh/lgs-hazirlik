﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using LgsKocu.Entity.Database;

namespace LgsKocu.Business.Concrete
{
    public static class UyeService
    {
        private static readonly DatabaseEntities DB = new DatabaseEntities();

        public static List<Uye> List()
        {
            var result = DB.Uye.ToList();
            return result;
        }

        public static Uye Detail(int id)
        {
            if(id == 0) return new Uye();

            var result = DB.Uye.Find(id);
            return result;
        }

        public static Uye Detail(string email)
        {
            if(string.IsNullOrEmpty(email)) return new Uye();
            email = email.Trim();
            if (string.IsNullOrEmpty(email)) return new Uye();

            var result = DB.Uye.Where(x => x.Email == email && x.CezaDurumu == false);
            if (result.Count() == 1) return result.First();
            else return new Uye();
        }

        public static int Login(string email, string pass)
        {
            if (string.IsNullOrEmpty(email)) return -99;
            email = email.Trim();
            if (string.IsNullOrEmpty(email)) return -99;

            if (string.IsNullOrEmpty(pass)) return -99;
            pass = pass.Trim();
            if (string.IsNullOrEmpty(pass)) return -99;

            var result = DB.Uye.Where(x => x.Email == email && x.Sifre == pass);
            if (!result.Any()) return -99;
            else
            {
                Uye uye = result.First();
                if (uye.CezaDurumu == false)
                {
                    System.Web.HttpContext.Current.Session["uid"] = uye.Id;
                    System.Web.HttpContext.Current.Session["ufull"] = uye.Ad + " " + uye.Soyad;
                    System.Web.HttpContext.Current.Session["uad"] = uye.Ad;
                    System.Web.HttpContext.Current.Session["usoyad"] = uye.Soyad;
                    return uye.Id;
                }
                if (uye.CezaDurumu == true) return -1;
                return 0;
            }
        }

        public static void Logout()
        {
            System.Web.HttpContext.Current.Session["uid"] = null;
            System.Web.HttpContext.Current.Session["ufull"] = null;
            System.Web.HttpContext.Current.Session["uad"] = null;
            System.Web.HttpContext.Current.Session["usoyad"] = null;
        }

        public static int Insert(Uye model)
        {
            var check = Detail(model.Email);
            if (check.Id != 0) return 0;

            model.CezaDurumu = false;
            model.KayitTarihi = DateTime.Now;
            DB.Uye.Add(model);
            DB.SaveChanges();
            
            return model.Id;
        }

        public static int Update(Uye model)
        {
            var check = Detail(model.Email);
            if (check.Id == 1) return 0;
            check.Ad = model.Ad;
            check.CezaDurumu = model.CezaDurumu;
            check.DogumTarihi = model.DogumTarihi;
            check.Email = model.Email;
            check.KayitTarihi = model.KayitTarihi;
            check.Sifre = model.Sifre;
            check.Soyad = model.Soyad;
            check.TcKimlik = model.TcKimlik;
            check.Telefon = model.Telefon;
            check.Cinsiyet = model.Cinsiyet;
            DB.SaveChanges();

            return model.Id;
        }

        public static int UpdateProfile(Uye model)
        {
            var check = Detail(model.Email);
            if (check.Id == 1) return 0;

            check.Ad = model.Ad;
            check.Soyad = model.Soyad;
            check.DogumTarihi = model.DogumTarihi;
            check.TcKimlik = model.TcKimlik;
            check.Telefon = model.Telefon;
            check.Cinsiyet = model.Cinsiyet;

            DB.SaveChanges();

            return model.Id;
        }

        public static int ChangePass(int id, string pass)
        {
            var model = Detail(id);

            model.Sifre = pass;
            DB.SaveChanges();

            return model.Id;
        }

        public static int ChangeMail(int id, string mail)
        {
            var model = Detail(id);

            model.Email = mail;
            DB.SaveChanges();

            return model.Id;
        }

        public static bool Delete(int id)
        {
            var model = Detail(id);
            DB.Uye.Remove(model);
            DB.SaveChanges();

            return Detail(id) == null;
        }

        public static int CezaActive(int id)
        {
            var model = Detail(id);
            model.CezaDurumu = true;
            DB.SaveChanges();

            return model.Id;
        }

        public static int CezePassive(int id)
        {
            var model = Detail(id);
            model.CezaDurumu = false;
            DB.SaveChanges();

            return model.Id;
        }
    }
}