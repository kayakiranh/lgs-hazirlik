﻿using LgsKocu.Entity.Database;
using System.Collections.Generic;
using System.Linq;

namespace LgsKocu.Business.Concrete
{
    public static class CekilisService
    {
        private static readonly DatabaseEntities DB = new DatabaseEntities();

        public static List<Cekilis> List()
        {
            var result = DB.Cekilis.ToList();
            return result;
        }

        public static Cekilis Detail(int id)
        {
            if (id == 0) return new Cekilis();

            var result = DB.Cekilis.Find(id);
            return result;
        }

        public static Cekilis Detail(string seo)
        {
            if (string.IsNullOrEmpty(seo)) return new Cekilis();
            seo = seo.Trim();
            if (string.IsNullOrEmpty(seo)) return new Cekilis();

            var result = DB.Cekilis.Where(x => x.Seo == seo && x.Durum == true);
            if (result.Count() == 1) return result.First();
            else return new Cekilis();
        }

        public static int Insert(Cekilis model)
        {
            var check = Detail(model.Seo);
            if (check.Id != 0) return 0;

            model.Durum = false;
            DB.Cekilis.Add(model);
            DB.SaveChanges();

            return model.Id;
        }

        public static int Update(Cekilis model)
        {
            var check = Detail(model.Seo);
            if (check.Id == 1) return 0;

            check.Ad = model.Ad;
            check.Seo = model.Seo;
            check.Aciklama = model.Aciklama;
            check.BaslangiTarihi = model.BaslangiTarihi;
            check.BitisTarihi = model.BitisTarihi;
            check.FacebookLink = model.FacebookLink;
            check.Ozet = model.Ozet;
            check.InstagramLink = model.InstagramLink;
            check.Gorsel = model.Gorsel;
            check.KatilimciSayisi = model.KatilimciSayisi;
            DB.SaveChanges();

            return model.Id;
        }

        public static bool Delete(int id)
        {
            var data = Detail(id);
            DB.Cekilis.Remove(data);
            DB.SaveChanges();

            return Detail(id) == null;
        }

        public static int Active(int id)
        {
            var data = Detail(id);
            data.Durum = true;
            DB.SaveChanges();

            return data.Id;

        }

        public static int Passive(int id)
        {
            var data = Detail(id);
            data.Durum = false;
            DB.SaveChanges();

            return data.Id;
        }

    }
}