﻿using System;
using System.Net;
using System.Net.NetworkInformation;
using System.Web;

namespace LgsKocu.Business.Helper
{
    public static class VisitorHelper
    {
        public static string GetIp()
        {
            string ipAddress = "";
            string hostname = Environment.MachineName;
            IPHostEntry host = Dns.GetHostEntry(hostname);
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                {
                    ipAddress = Convert.ToString(ip);
                }
            }

            return ipAddress;
        }

        public static string GetMac()
        {
            NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();
            String sMacAddress = string.Empty;
            foreach (NetworkInterface adapter in nics)
            {
                if (sMacAddress == string.Empty)
                {
                    sMacAddress = adapter.GetPhysicalAddress().ToString();
                }
            }
            return sMacAddress;
        }

        public static int GetUyeId()
        {
            var sessionId = HttpContext.Current.Session["id"];

            if (sessionId == null) return 0;

            try { return Convert.ToInt32(sessionId); }
            catch { return 0; }
        }
    }
}