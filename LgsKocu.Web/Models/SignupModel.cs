﻿namespace LgsKocu.Web.Models
{
    public class SignupModel
    {
        public string suAd { get; set; }
        public string suSoyad { get; set; }
        public string suEmail { get; set; }
        public string suSifre { get; set; }
        public string suTelefon { get; set; }
        public string suAdres { get; set; }
    }
}