//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LgsKocu.Web.Database
{
    using System;
    using System.Collections.Generic;
    
    public partial class CekilisKatilimci
    {
        public int Id { get; set; }
        public int CekilisId { get; set; }
        public int UyeId { get; set; }
        public System.DateTime KatilmaTarihi { get; set; }
        public string IpAdres { get; set; }
    
        public virtual Cekilis Cekilis { get; set; }
        public virtual Uye Uye { get; set; }
    }
}
