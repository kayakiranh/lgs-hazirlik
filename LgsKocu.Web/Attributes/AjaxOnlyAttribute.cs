﻿using System;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace LgsKocu.Web.Attributes
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public sealed class AjaxOnlyAttribute : ActionMethodSelectorAttribute
    {
        public override bool IsValidForRequest(ControllerContext controllerContext, MethodInfo methodInfo)
        {
            if (controllerContext == null)
                throw new ArgumentNullException(nameof(controllerContext));

            return controllerContext.RequestContext.HttpContext.Request.IsAjaxRequest();
        }
    }
}