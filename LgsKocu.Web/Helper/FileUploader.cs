﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace LgsKocu.Web.Helper
{
    public static class FileUploader
    {
        public static string Upload(HttpPostedFileBase imageFile, string filePath)
        {
            if (imageFile != null)
            {
                if (imageFile.ContentLength > 0)
                {
                    var fileName = imageFile.FileName;
                    if (filePath == "Dosya")
                    {
                        fileName = GenerateSlug(fileName);
                    }
                    else
                    {
                        fileName = Guid.NewGuid().ToString() + DateTime.Now.ToString("YYYYMMDDHHMMSS"); ;
                    }

                    var path = Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~/Assets/Files/" + filePath + "/"), fileName);
                    imageFile.SaveAs(path);

                    return "/Assets/Files/" + filePath + "/" + fileName;
                }
            }
            return "";
        }

        private static string GenerateSlug(this string phrase)
        {
            string str = phrase.RemoveAccent().ToLower();
            // invalid chars           
            str = Regex.Replace(str, @"[^a-z0-9\s-]", "");
            // convert multiple spaces into one space   
            str = Regex.Replace(str, @"\s+", "_").Trim();
            // cut and trim 
            str = str.Substring(0, str.Length <= 45 ? str.Length : 45).Trim();
            str = Regex.Replace(str, @"\s", "-"); // hyphens   
            return str;
        }

        private static string RemoveAccent(this string txt)
        {
            byte[] bytes = System.Text.Encoding.GetEncoding("Cyrillic").GetBytes(txt);
            return System.Text.Encoding.ASCII.GetString(bytes);
        }
    }
}