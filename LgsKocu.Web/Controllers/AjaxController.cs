﻿using System;
using System.Web.Mvc;
using LgsKocu.Web.Attributes;
using LgsKocu.Web.Database;
using LgsKocu.Web.Models;
using System.Linq;

namespace LgsKocu.Web.Controllers
{
    public class AjaxController : Controller
    {
        private readonly LgsHazirlikEntities _db = new LgsHazirlikEntities();

        [HttpPost]
        [AjaxOnly]
        [Route("ajax/post-uye-ol")]
        public ActionResult SubmitSignupForm(SignupModel signUpModel)
        {
            if (string.IsNullOrEmpty(signUpModel.suAd) || string.IsNullOrEmpty(signUpModel.suSoyad) || string.IsNullOrEmpty(signUpModel.suEmail) || string.IsNullOrEmpty(signUpModel.suSifre)) return Json(new { result = "error", msg = "Bilgilerde hata var. Lütfen üyelik formunu kontrol ediniz." });

            int result = _db.Uye.Count(x => x.Email == signUpModel.suEmail);
            if (result > 0) return Json(new { result = "already", msg = "Zaten üyesiniz. Email adresiniz ve şifrenizle giriş yapabilirsiniz." });

            Uye uye = new Uye() { Ad = signUpModel.suAd, Soyad = signUpModel.suSoyad, CezaDurumu = false, Email = signUpModel.suEmail, Adres = signUpModel.suAdres, Telefon = signUpModel.suTelefon, Sifre = signUpModel.suSifre,KayitTarihi = DateTime.Now};

            _db.Uye.Add(uye);
            _db.SaveChanges();

            Session["uyeId"] = uye.Id;
            Session["uyeAd"] = uye.Ad;
            Session["uyeSoyad"] = uye.Soyad;

            return Json(new { result="ok", msg = "Zaten üyesiniz. Email adresiniz ve şifrenizle giriş yapabilirsiniz." });
        }

        [HttpPost]
        [AjaxOnly]
        [Route("ajax/post-giris-yap")]
        public ActionResult SubmitLoginForm(LoginModel loginModel)
        {
            Uye uye = _db.Uye.Where(x => x.Email == loginModel.lmMail && x.Sifre == loginModel.lmPass).SingleOrDefault();

            if (uye == null)
            {
                Session["uyeId"] = null;
                Session["uyeAd"] = null;
                Session["uyeSoyad"] = null;
                return Json(new { result = "error", msg = "Formu hatalı doldurdunuz veya kullanıcı bilgilerini hatalı girdiniz." });
            }

            if (uye.CezaDurumu == true)
            {
                Session["uyeId"] = null;
                Session["uyeAd"] = null;
                Session["uyeSoyad"] = null;
                return Json(new { result = "error", msg = "Şüpheli işlemlerinizden kaynaklı, hesabınız geçici olarak erişime kapatılmıştır." });
            }

            Session["uyeId"] = uye.Id;
            Session["uyeAd"] = uye.Ad;
            Session["uyeSoyad"] = uye.Soyad;

            return Json(new { result = "ok", msg = "" });
        }

        [HttpPost]
        [AjaxOnly]
        [Route("ajax/post-cekilise-katil")]
        public ActionResult JoinRaffle(string seo)
        {
            var session = Session["uyeId"];
            if (session != null)
            {
                int uyeId = Convert.ToInt32(session);

                Cekilis cekilis = _db.Cekilis.Where(x =>x.Seo == seo && x.Durum == true && x.BaslangiTarihi <= DateTime.Now &&x.BitisTarihi >= DateTime.Now).SingleOrDefault();
                if(cekilis == null) return Json(new { msg = "Katıldığınız çekiliş kaldırıldı veya son katılma tarihi geçti. Lütfen yeni çekilişleri takip ediniz." });

                int cekilisId = cekilis.Id;

                int result = _db.CekilisKatilimci.Where(x => x.UyeId == uyeId && x.CekilisId == cekilisId).Count();
                if (result > 0) return Json(new { msg = "Bu çekilişe daha önce katıldınız. Çekiliş bitince bilgi maili gönderilecektir." });
                else
                {
                    CekilisKatilimci cekilisKatilimci = new CekilisKatilimci() { IpAdres = "", CekilisId = cekilisId, UyeId = uyeId, KatilmaTarihi = DateTime.Now };
                    _db.CekilisKatilimci.Add(cekilisKatilimci);
                    _db.SaveChanges();
                    return Json(new { msg = "Tebrikler, Çekilişe katıldınız. Çekiliş bitince bilgi maili gönderilecektir." });
                }
            }

            return Json(new { msg = "Üyelik yada sayfa bilgisinde hata var. Üye girişi yapmadınız veya hatalı çekilişe katıldınız." });
        }

        [HttpPost]
        [AjaxOnly]
        [Route("indir/{fileId}")]
        public ActionResult DownloadFile(int fileId)
        {
            var file = _db.Dosya.Find(fileId);
            if (file != null)
            {
                string fullName = Server.MapPath("~" + "");

                byte[] fileBytes = GetFile(fullName);
                return File(
                    fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, "");
            }

            return null;

        }

        private byte[] GetFile(string s)
        {
            System.IO.FileStream fs = System.IO.File.OpenRead(s);
            byte[] data = new byte[fs.Length];
            int br = fs.Read(data, 0, data.Length);
            if (br != fs.Length)
                throw new System.IO.IOException(s);
            return data;
        }
    }
}