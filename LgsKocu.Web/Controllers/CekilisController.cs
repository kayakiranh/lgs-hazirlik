﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Security.Cryptography.X509Certificates;
using System.Web.Mvc;
using LgsKocu.Web.Database;
using System.Linq;

namespace LgsKocu.Web.Controllers
{
    [RoutePrefix("cekilis")]
    [Route("{action = index}")]
    public class CekilisController : Controller
    {
        private readonly LgsHazirlikEntities _db = new LgsHazirlikEntities();

        [Route()]
        public ActionResult Index()
        {
            List<Cekilis> data = _db.Cekilis.Include(c => c.CekilisKatilimci).ToList();
            return View(data);
        }

        [Route("{seo}")]
        public ActionResult Detail(string seo)
        {
            Cekilis cekilis = _db.Cekilis.Where(x => x.Seo == seo).SingleOrDefault();

            if (cekilis == null) return RedirectToAction("index", "Error");

            if (cekilis.Durum == false) ViewBag.CanJoin = "removed";
            else if (cekilis.Durum == true && cekilis.BitisTarihi <= DateTime.Now) ViewBag.CanJoin = "done";
            else if (cekilis.Durum == true && cekilis.BaslangiTarihi >= DateTime.Now) ViewBag.CanJoin = "early";
            else if (cekilis.CekilisKatilimci.Count >= cekilis.KatilimciSayisi) ViewBag.CanJoin = "full";
            else
            {
                var uye = Session["uyeId"];
                if (uye == null) ViewBag.CanJoin = "onlymember";
                else
                {
                    int uyeId = Convert.ToInt32(uye);
                    int result = _db.CekilisKatilimci.Where(x => x.UyeId == uyeId && x.CekilisId == cekilis.Id).Count();
                    if (result > 0) ViewBag.CanJoin = "alreadyjoin";
                    else ViewBag.CanJoin = "canjoin";
                }
            }

            return View(cekilis);
        }

    }
}