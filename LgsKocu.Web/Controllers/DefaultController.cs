﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LgsKocu.Web.Database;

namespace LgsKocu.Web.Controllers
{
    [RoutePrefix("")]
    [Route("{action = index}")]
    public class DefaultController : Controller
    {
        private readonly LgsHazirlikEntities _db = new LgsHazirlikEntities();

        [Route()]
        public ActionResult Index()
        {
            return View();
        }

        [Route("giris-yap")]
        public ActionResult Login()
        {
            return View();
        }

        [Route("guvenli-cikis")]
        public ActionResult Logout()
        {
            Session["uyeId"] = null;
            Session["uyeAd"] = null;
            Session["uyeSoyad"] = null;

            return View();
        }

        [Route("uye-ol")]
        public ActionResult Signup()
        {
            return View();
        }

        [Route("sifremi-unuttum")]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        [Route("hakkimizda")]
        public ActionResult AboutUs()
        {
            return View();
        }

        [Route("iletisim")]
        public ActionResult Contact()
        {
            return View();
        }

        [Route("gizlilik-sozlesmesi")]
        public ActionResult PrivacyContact()
        {
            return View();
        }

        [Route("siniflar")]
        public ActionResult Siniflar()
        {
            ViewBag.SekizDers = _db.Ders.Where(x => x.SinifId == 1).ToList();
            ViewBag.YediDers = _db.Ders.Where(x => x.SinifId == 2).ToList();
            ViewBag.AltiDers = _db.Ders.Where(x => x.SinifId == 3).ToList();
            ViewBag.BesDers = _db.Ders.Where(x => x.SinifId == 4).ToList();

            ViewBag.SekizDosya = _db.Dosya.Where(x => x.SinifId == 1).Take(6).ToList();
            ViewBag.YediDosya = _db.Dosya.Where(x => x.SinifId == 2).Take(6).ToList();
            ViewBag.AltiDosya = _db.Dosya.Where(x => x.SinifId == 3).Take(6).ToList();
            ViewBag.BesDosya = _db.Dosya.Where(x => x.SinifId == 4).Take(6).ToList();
            return View();
        }

        [Route("sekizinci-sinif")]
        public ActionResult SekizinciSinif()
        {
            var data = _db.Sinif.Where(x => x.Seo == "sekizinci-sinif").SingleOrDefault();
            ViewBag.Yazilar = _db.Yazi.Where(x => x.SinifId == 1).ToList();
            return View(data);
        }

        [Route("yedinci-sinif")]
        public ActionResult YedinciSinif()
        {
            var data = _db.Sinif.Where(x => x.Seo == "yedinci-sinif").SingleOrDefault();
            ViewBag.Yazilar = _db.Yazi.Where(x => x.SinifId == 1).ToList();
            return View(data);
        }

        [Route("altinci-sinif")]
        public ActionResult AltinciSinif()
        {
            var data = _db.Sinif.Where(x => x.Seo == "altinci-sinif").SingleOrDefault();
            ViewBag.Yazilar = _db.Yazi.Where(x => x.SinifId == 1).ToList();
            return View(data);
        }

        [Route("besinci-sinif")]
        public ActionResult BesinciSinif()
        {
            var data = _db.Sinif.Where(x => x.Seo == "besinci-sinif").SingleOrDefault();
            ViewBag.Yazilar = _db.Yazi.Where(x => x.SinifId == 1).ToList();
            return View(data);
        }

        //[Route("profil")]
        //public ActionResult Profile()
        //{
        //    //if (Session["uid"] == null) RedirectToAction("Index", "Default");

        //    //int uyeId = Convert.ToInt32(Session["uid"].ToString());
        //    //Uye uye = UyeService.Detail(uyeId);

        //    return View();
        //}

        //[Route("{sinif}/{ders}")]
        //public ActionResult Ders()
        //{
        //    var data = _db.Sinif.Where(x => x.Seo == "besinci-sinif").SingleOrDefault();
        //    ViewBag.Yazilar = _db.Yazi.Where(x => x.SinifId == 1).ToList();
        //    return View(data);
        //}

        //[Route("{sinif}/{ders}/{konu}")]
        //public ActionResult Konu()
        //{
        //    var data = _db.Sinif.Where(x => x.Seo == "besinci-sinif").SingleOrDefault();
        //    ViewBag.Yazilar = _db.Yazi.Where(x => x.SinifId == 1).ToList();
        //    return View(data);
        //}

        //[Route("download/{dosyaId}")]
        //public ActionResult Download(int dosyaId)
        //{
        //    var data = _db.Sinif.Where(x => x.Seo == "besinci-sinif").SingleOrDefault();
        //    ViewBag.Yazilar = _db.Yazi.Where(x => x.SinifId == 1).ToList();
        //    return View(data);
        //}

    }
}