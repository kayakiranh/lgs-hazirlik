﻿using System.Web.Mvc;

namespace LgsKocu.Web.Controllers
{
    [Route("{action = index}")]
    public class ErrorController : Controller
    {
        [Route("sayfa-bulunamadi")]
        public ActionResult Index()
        {
            return View();
        }
    }
}