﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using LgsKocu.Web.Database;

namespace LgsKocu.Web.Areas.Admin.Controllers
{
    public class AdminDersController : Controller
    {
        private readonly LgsHazirlikEntities db = new LgsHazirlikEntities();

        // GET: Admin/AdminDers
        public ActionResult Index()
        {
            var ders = db.Ders.Include(d => d.Sinif);
            return View(ders.ToList());
        }

        // GET: Admin/AdminDers/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var ders = db.Ders.Find(id);
            if (ders == null) return HttpNotFound();
            return View(ders);
        }

        // GET: Admin/AdminDers/Create
        public ActionResult Create()
        {
            ViewBag.SinifId = new SelectList(db.Sinif, "Id", "Ad");
            return View();
        }

        // POST: Admin/AdminDers/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Ad,Seo,SinifId")] Ders ders)
        {
            if (ModelState.IsValid)
            {
                db.Ders.Add(ders);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.SinifId = new SelectList(db.Sinif, "Id", "Ad");
            return View(ders);
        }

        // GET: Admin/AdminDers/Edit/5
        public ActionResult Edit(int? id)
        {
            ViewBag.SinifId = new SelectList(db.Sinif, "Id", "Ad");
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var ders = db.Ders.Find(id);
            if (ders == null) return HttpNotFound();
            return View(ders);
        }

        // POST: Admin/AdminDers/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Ad,Seo,SinifId")] Ders ders)
        {
            if (ModelState.IsValid)
            {
                db.Entry(ders).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.SinifId = new SelectList(db.Sinif, "Id", "Ad");
            return View(ders);
        }

        // GET: Admin/AdminDers/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var ders = db.Ders.Find(id);
            if (ders == null) return HttpNotFound();
            return View(ders);
        }

        // POST: Admin/AdminDers/Delete/5
        [HttpPost]
        [ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var ders = db.Ders.Find(id);
            db.Ders.Remove(ders);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing) db.Dispose();
            base.Dispose(disposing);
        }
    }
}