﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using LgsKocu.Web.Database;

namespace LgsKocu.Web.Areas.Admin.Controllers
{
    public class AdminCekilisKatilimciController : Controller
    {
        private readonly LgsHazirlikEntities db = new LgsHazirlikEntities();

        public ActionResult Index(int? id)
        {
            if (id == null)
            {
                var cekilisKatilimci = db.CekilisKatilimci.Include(c => c.Cekilis).Include(c => c.Uye);
                return View(cekilisKatilimci.ToList());
            }
            else
            {
                var cekilisKatilimci = db.CekilisKatilimci.Where(x => x.CekilisId == id).Include(c => c.Cekilis).Include(c => c.Uye);
                return View(cekilisKatilimci.ToList());
            }
        }

        public ActionResult Details(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var cekilisKatilimci = db.CekilisKatilimci.Find(id);
            if (cekilisKatilimci == null) return HttpNotFound();
            return View(cekilisKatilimci);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var cekilisKatilimci = db.CekilisKatilimci.Find(id);
            if (cekilisKatilimci == null) return HttpNotFound();
            return View(cekilisKatilimci);
        }

        [HttpPost]
        [ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var cekilisKatilimci = db.CekilisKatilimci.Find(id);
            db.CekilisKatilimci.Remove(cekilisKatilimci);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing) db.Dispose();
            base.Dispose(disposing);
        }
    }
}