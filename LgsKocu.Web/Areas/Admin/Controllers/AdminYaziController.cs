﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using LgsKocu.Web.Database;

namespace LgsKocu.Web.Areas.Admin.Controllers
{
    public class AdminYaziController : Controller
    {
        private readonly LgsHazirlikEntities db = new LgsHazirlikEntities();

        public ActionResult Index()
        {
            var yazi = db.Yazi.Include(y => y.Ders).Include(y => y.Konu).Include(y => y.Sinif);
            return View(yazi.ToList());
        }

        public ActionResult Details(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var yazi = db.Yazi.Find(id);
            if (yazi == null) return HttpNotFound();
            return View(yazi);
        }

        public ActionResult Create()
        {
            ViewBag.DersId = new SelectList(db.Ders, "Id", "Ad");
            ViewBag.KonuId = new SelectList(db.Konu, "Id", "Ad");
            ViewBag.SinifId = new SelectList(db.Sinif, "Id", "Ad");
            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Ad,Seo,Icerik,SinifId,KonuId,DersId")]
            Yazi yazi)
        {
            if (ModelState.IsValid)
            {
                db.Yazi.Add(yazi);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.DersId = new SelectList(db.Ders, "Id", "Ad", yazi.DersId);
            ViewBag.KonuId = new SelectList(db.Konu, "Id", "Ad", yazi.KonuId);
            ViewBag.SinifId = new SelectList(db.Sinif, "Id", "Ad", yazi.SinifId);
            return View(yazi);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var yazi = db.Yazi.Find(id);
            if (yazi == null) return HttpNotFound();
            ViewBag.DersId = new SelectList(db.Ders, "Id", "Ad", yazi.DersId);
            ViewBag.KonuId = new SelectList(db.Konu, "Id", "Ad", yazi.KonuId);
            ViewBag.SinifId = new SelectList(db.Sinif, "Id", "Ad", yazi.SinifId);
            return View(yazi);
        }

        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Ad,Seo,Icerik,SinifId,KonuId,DersId")]
            Yazi yazi)
        {
            if (ModelState.IsValid)
            {
                db.Entry(yazi).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.DersId = new SelectList(db.Ders, "Id", "Ad", yazi.DersId);
            ViewBag.KonuId = new SelectList(db.Konu, "Id", "Ad", yazi.KonuId);
            ViewBag.SinifId = new SelectList(db.Sinif, "Id", "Ad", yazi.SinifId);
            return View(yazi);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var yazi = db.Yazi.Find(id);
            if (yazi == null) return HttpNotFound();
            return View(yazi);
        }

        [HttpPost]
        [ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var yazi = db.Yazi.Find(id);
            db.Yazi.Remove(yazi);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing) db.Dispose();
            base.Dispose(disposing);
        }
    }
}