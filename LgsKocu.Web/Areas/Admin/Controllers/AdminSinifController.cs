﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using LgsKocu.Web.Database;

namespace LgsKocu.Web.Areas.Admin.Controllers
{
    public class AdminSinifController : Controller
    {
        private readonly LgsHazirlikEntities db = new LgsHazirlikEntities();

        // GET: Admin/AdminSinif
        public ActionResult Index()
        {
            return View(db.Sinif.ToList());
        }

        // GET: Admin/AdminSinif/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var sinif = db.Sinif.Find(id);
            if (sinif == null) return HttpNotFound();
            return View(sinif);
        }

        // GET: Admin/AdminSinif/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/AdminSinif/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Ad,Seo")] Sinif sinif)
        {
            if (ModelState.IsValid)
            {
                db.Sinif.Add(sinif);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(sinif);
        }

        // GET: Admin/AdminSinif/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var sinif = db.Sinif.Find(id);
            if (sinif == null) return HttpNotFound();
            return View(sinif);
        }

        // POST: Admin/AdminSinif/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Ad,Seo")] Sinif sinif)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sinif).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(sinif);
        }

        // GET: Admin/AdminSinif/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var sinif = db.Sinif.Find(id);
            if (sinif == null) return HttpNotFound();
            return View(sinif);
        }

        // POST: Admin/AdminSinif/Delete/5
        [HttpPost]
        [ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var sinif = db.Sinif.Find(id);
            db.Sinif.Remove(sinif);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing) db.Dispose();
            base.Dispose(disposing);
        }
    }
}