﻿using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LgsKocu.Web.Database;

namespace LgsKocu.Web.Areas.Admin.Controllers
{
    public class AdminCevapController : Controller
    {
        private readonly LgsHazirlikEntities db = new LgsHazirlikEntities();

        public ActionResult Index()
        {
            var cevap = db.Cevap.Include(c => c.Soru).Include(c => c.Uye);
            return View(cevap.ToList());
        }

        public ActionResult Details(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var cevap = db.Cevap.Find(id);
            if (cevap == null) return HttpNotFound();
            return View(cevap);
        }

        public ActionResult Create()
        {
            ViewBag.SoruId = new SelectList(db.Soru, "Id", "Baslik");
            ViewBag.UyeId = new SelectList(db.Uye, "Id", "Ad");
            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Cevap cevap, HttpPostedFileBase imageFile)
        {
            if (ModelState.IsValid)
            {
                if (imageFile != null)
                {
                    if (imageFile.ContentLength > 0)
                    {
                        var path = Path.Combine(Server.MapPath("~/Assets/Files/Cevap/"), imageFile.FileName);
                        imageFile.SaveAs(path);

                        cevap.Gorsel = "/Assets/Files/Cevap/" + imageFile.FileName;
                    }
                }

                db.Cevap.Add(cevap);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.SoruId = new SelectList(db.Soru, "Id", "Baslik", cevap.SoruId);
            ViewBag.UyeId = new SelectList(db.Uye, "Id", "Ad", cevap.UyeId);
            return View(cevap);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var cevap = db.Cevap.Find(id);
            if (cevap == null) return HttpNotFound();
            ViewBag.SoruId = new SelectList(db.Soru, "Id", "Baslik", cevap.SoruId);
            ViewBag.UyeId = new SelectList(db.Uye, "Id", "Ad", cevap.UyeId);
            return View(cevap);
        }

        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Cevap cevap, HttpPostedFileBase imageFile)
        {
            if (ModelState.IsValid)
            {
                if (imageFile != null)
                {
                    if (imageFile.ContentLength > 0)
                    {
                        var path = Path.Combine(Server.MapPath("~/Assets/Files/Cevap/"), imageFile.FileName);
                        imageFile.SaveAs(path);

                        cevap.Gorsel = "/Assets/Files/Cevap/" + imageFile.FileName;
                    }
                }
                else
                {
                    cevap.Gorsel = db.Cevap.Find(cevap.Id).Gorsel;
                }

                db.Entry(cevap).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.SoruId = new SelectList(db.Soru, "Id", "Baslik", cevap.SoruId);
            ViewBag.UyeId = new SelectList(db.Uye, "Id", "Ad", cevap.UyeId);
            return View(cevap);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var cevap = db.Cevap.Find(id);
            if (cevap == null) return HttpNotFound();
            return View(cevap);
        }

        [HttpPost]
        [ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var cevap = db.Cevap.Find(id);
            db.Cevap.Remove(cevap);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing) db.Dispose();
            base.Dispose(disposing);
        }
    }
}