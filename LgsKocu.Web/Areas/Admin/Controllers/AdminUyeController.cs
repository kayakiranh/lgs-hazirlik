﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using LgsKocu.Web.Database;

namespace LgsKocu.Web.Areas.Admin.Controllers
{
    public class AdminUyeController : Controller
    {
        private readonly LgsHazirlikEntities db = new LgsHazirlikEntities();

        public ActionResult Index()
        {
            return View(db.Uye.ToList());
        }

        public ActionResult Details(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var uye = db.Uye.Find(id);
            if (uye == null) return HttpNotFound();
            return View(uye);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var uye = db.Uye.Find(id);
            if (uye == null) return HttpNotFound();
            return View(uye);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include =
                "Id,Ad,Soyad,Email,KayitTarihi,Sifre,DogumTarihi,EnYakinPtt,Telefon,Cinsiyet,TcKimlik,CezaDurumu")]
            Uye uye)
        {
            if (ModelState.IsValid)
            {
                db.Entry(uye).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(uye);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var uye = db.Uye.Find(id);
            if (uye == null) return HttpNotFound();
            return View(uye);
        }

        [HttpPost]
        [ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var uye = db.Uye.Find(id);
            db.Uye.Remove(uye);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing) db.Dispose();
            base.Dispose(disposing);
        }
    }
}