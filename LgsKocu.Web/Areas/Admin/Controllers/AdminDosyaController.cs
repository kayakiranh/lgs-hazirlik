﻿using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LgsKocu.Web.Database;

namespace LgsKocu.Web.Areas.Admin.Controllers
{
    public class AdminDosyaController : Controller
    {
        private readonly LgsHazirlikEntities db = new LgsHazirlikEntities();

        // GET: Admin/AdminDosya
        public ActionResult Index()
        {
            var dosya = db.Dosya.Include(d => d.Ders).Include(d => d.Konu).Include(d => d.Sinif);
            return View(dosya.ToList());
        }

        // GET: Admin/AdminDosya/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var dosya = db.Dosya.Find(id);
            if (dosya == null) return HttpNotFound();
            return View(dosya);
        }

        // GET: Admin/AdminDosya/Create
        public ActionResult Create()
        {
            ViewBag.DersId = new SelectList(db.Ders, "Id", "Ad");
            ViewBag.KonuId = new SelectList(db.Konu, "Id", "Ad");
            ViewBag.SinifId = new SelectList(db.Sinif, "Id", "Ad");
            return View();
        }

        // POST: Admin/AdminDosya/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Dosya dosya, HttpPostedFileBase imageFile)
        {
            if (ModelState.IsValid)
            {
                if (imageFile != null)
                {
                    if (imageFile.ContentLength > 0)
                    {
                        var path = Path.Combine(Server.MapPath("~/Assets/Files/Dosya/"), imageFile.FileName);
                        imageFile.SaveAs(path);

                        dosya.Link = "/Assets/Files/Dosya/" + imageFile.FileName;

                        db.Dosya.Add(dosya);
                        db.SaveChanges();
                        return RedirectToAction("Index");
                    }
                }

            }

            ViewBag.DersId = new SelectList(db.Ders, "Id", "Ad", dosya.DersId);
            ViewBag.KonuId = new SelectList(db.Konu, "Id", "Ad", dosya.KonuId);
            ViewBag.SinifId = new SelectList(db.Sinif, "Id", "Ad", dosya.SinifId);
            return View(dosya);
        }

        // GET: Admin/AdminDosya/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var dosya = db.Dosya.Find(id);
            if (dosya == null) return HttpNotFound();
            ViewBag.DersId = new SelectList(db.Ders, "Id", "Ad", dosya.DersId);
            ViewBag.KonuId = new SelectList(db.Konu, "Id", "Ad", dosya.KonuId);
            ViewBag.SinifId = new SelectList(db.Sinif, "Id", "Ad", dosya.SinifId);
            return View(dosya);
        }

        // POST: Admin/AdminDosya/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Dosya dosya, HttpPostedFileBase imageFile)
        {
            if (ModelState.IsValid)
            {
                if (imageFile != null)
                {
                    if (imageFile.ContentLength > 0)
                    {
                        var path = Path.Combine(Server.MapPath("~/Assets/Files/Dosya/"), imageFile.FileName);
                        imageFile.SaveAs(path);

                        dosya.Link = "/Assets/Files/Dosya/" + imageFile.FileName;
                    }
                }
                else
                {
                    dosya.Link = db.Dosya.Find(dosya.Id).Link;
                }

                db.Entry(dosya).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.DersId = new SelectList(db.Ders, "Id", "Ad", dosya.DersId);
            ViewBag.KonuId = new SelectList(db.Konu, "Id", "Ad", dosya.KonuId);
            ViewBag.SinifId = new SelectList(db.Sinif, "Id", "Ad", dosya.SinifId);
            return View(dosya);
        }

        // GET: Admin/AdminDosya/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var dosya = db.Dosya.Find(id);
            if (dosya == null) return HttpNotFound();
            return View(dosya);
        }

        // POST: Admin/AdminDosya/Delete/5
        [HttpPost]
        [ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var dosya = db.Dosya.Find(id);
            db.Dosya.Remove(dosya);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing) db.Dispose();
            base.Dispose(disposing);
        }
    }
}