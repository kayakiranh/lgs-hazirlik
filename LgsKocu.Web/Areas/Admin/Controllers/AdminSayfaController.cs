﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using LgsKocu.Web.Database;

namespace LgsKocu.Web.Areas.Admin.Controllers
{
    public class AdminSayfaController : Controller
    {
        private readonly LgsHazirlikEntities db = new LgsHazirlikEntities();

        // GET: Admin/AdminSayfa
        public ActionResult Index()
        {
            return View(db.Sayfa.ToList());
        }

        // GET: Admin/AdminSayfa/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var sayfa = db.Sayfa.Find(id);
            if (sayfa == null) return HttpNotFound();
            return View(sayfa);
        }

        // GET: Admin/AdminSayfa/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/AdminSayfa/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Ad,Seo,Icerik")] Sayfa sayfa)
        {
            if (ModelState.IsValid)
            {
                db.Sayfa.Add(sayfa);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(sayfa);
        }

        // GET: Admin/AdminSayfa/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var sayfa = db.Sayfa.Find(id);
            if (sayfa == null) return HttpNotFound();
            return View(sayfa);
        }

        // POST: Admin/AdminSayfa/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Ad,Seo,Icerik")] Sayfa sayfa)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sayfa).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(sayfa);
        }

        // GET: Admin/AdminSayfa/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var sayfa = db.Sayfa.Find(id);
            if (sayfa == null) return HttpNotFound();
            return View(sayfa);
        }

        // POST: Admin/AdminSayfa/Delete/5
        [HttpPost]
        [ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var sayfa = db.Sayfa.Find(id);
            db.Sayfa.Remove(sayfa);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing) db.Dispose();
            base.Dispose(disposing);
        }
    }
}