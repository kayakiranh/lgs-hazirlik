﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LgsKocu.Web.Database;

namespace LgsKocu.Web.Areas.Admin.Controllers
{
    public class AdminKonuController : Controller
    {
        private LgsHazirlikEntities db = new LgsHazirlikEntities();

        // GET: Admin/AdminKonuu
        public ActionResult Index()
        {
            var konu = db.Konu.Include(k => k.Ders).Include(k => k.Sinif);
            return View(konu.ToList());
        }

        // GET: Admin/AdminKonuu/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Konu konu = db.Konu.Find(id);
            if (konu == null)
            {
                return HttpNotFound();
            }
            return View(konu);
        }

        // GET: Admin/AdminKonuu/Create
        public ActionResult Create()
        {
            ViewBag.DersId = new SelectList(db.Ders, "Id", "Ad");
            ViewBag.SinifId = new SelectList(db.Sinif, "Id", "Ad");
            return View();
        }

        // POST: Admin/AdminKonuu/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Ad,Seo,SinifId,DersId")] Konu konu)
        {
            if (ModelState.IsValid)
            {
                db.Konu.Add(konu);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.DersId = new SelectList(db.Ders, "Id", "Ad", konu.DersId);
            ViewBag.SinifId = new SelectList(db.Sinif, "Id", "Ad", konu.SinifId);
            return View(konu);
        }

        // GET: Admin/AdminKonuu/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Konu konu = db.Konu.Find(id);
            if (konu == null)
            {
                return HttpNotFound();
            }
            ViewBag.DersId = new SelectList(db.Ders, "Id", "Ad", konu.DersId);
            ViewBag.SinifId = new SelectList(db.Sinif, "Id", "Ad", konu.SinifId);
            return View(konu);
        }

        // POST: Admin/AdminKonuu/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Ad,Seo,SinifId,DersId")] Konu konu)
        {
            if (ModelState.IsValid)
            {
                db.Entry(konu).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.DersId = new SelectList(db.Ders, "Id", "Ad", konu.DersId);
            ViewBag.SinifId = new SelectList(db.Sinif, "Id", "Ad", konu.SinifId);
            return View(konu);
        }

        // GET: Admin/AdminKonuu/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Konu konu = db.Konu.Find(id);
            if (konu == null)
            {
                return HttpNotFound();
            }
            return View(konu);
        }

        // POST: Admin/AdminKonuu/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Konu konu = db.Konu.Find(id);
            db.Konu.Remove(konu);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
