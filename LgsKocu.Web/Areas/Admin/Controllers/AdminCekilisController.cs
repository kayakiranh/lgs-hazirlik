﻿using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LgsKocu.Web.Database;

namespace LgsKocu.Web.Areas.Admin.Controllers
{
    public class AdminCekilisController : Controller
    {
        private readonly LgsHazirlikEntities db = new LgsHazirlikEntities();

        public ActionResult Index()
        {
            var cekilis = db.Cekilis.Include(c => c.Ders).Include(c => c.Konu).Include(c => c.Sinif);
            return View(cekilis.ToList());
        }

        public ActionResult Details(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var cekilis = db.Cekilis.Find(id);
            if (cekilis == null) return HttpNotFound();
            return View(cekilis);
        }

        public ActionResult Create()
        {
            ViewBag.DersId = new SelectList(db.Ders, "Id", "Ad");
            ViewBag.KonuId = new SelectList(db.Konu, "Id", "Ad");
            ViewBag.SinifId = new SelectList(db.Sinif, "Id", "Ad");
            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Cekilis cekilis, HttpPostedFileBase imageFile)
        {
            if (ModelState.IsValid)
                if (imageFile != null)
                {
                    if (imageFile.ContentLength > 0)
                    {
                        var path = Path.Combine(Server.MapPath("~/Assets/Files/Cekilis/"), imageFile.FileName);
                        imageFile.SaveAs(path);

                        cekilis.Gorsel = "/Assets/Files/Cekilis/" + imageFile.FileName;

                        db.Cekilis.Add(cekilis);
                        db.SaveChanges();
                        return RedirectToAction("Index");
                    }
                }


            ViewBag.DersId = new SelectList(db.Ders, "Id", "Ad", cekilis.DersId);
            ViewBag.KonuId = new SelectList(db.Konu, "Id", "Ad", cekilis.KonuId);
            ViewBag.SinifId = new SelectList(db.Sinif, "Id", "Ad", cekilis.SinifId);
            return View(cekilis);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var cekilis = db.Cekilis.Find(id);
            if (cekilis == null) return HttpNotFound();
            ViewBag.DersId = new SelectList(db.Ders, "Id", "Ad", cekilis.DersId);
            ViewBag.KonuId = new SelectList(db.Konu, "Id", "Ad", cekilis.KonuId);
            ViewBag.SinifId = new SelectList(db.Sinif, "Id", "Ad", cekilis.SinifId);
            return View(cekilis);
        }

        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Cekilis cekilis, HttpPostedFileBase imageFile)
        {
            if (ModelState.IsValid)
            {
                if (imageFile != null)
                {
                    if (imageFile.ContentLength > 0)
                    {
                        var path = Path.Combine(Server.MapPath("~/Assets/Files/Cekilis/"), imageFile.FileName);
                        imageFile.SaveAs(path);

                        cekilis.Gorsel = "/Assets/Files/Cekilis/" + imageFile.FileName;
                    }
                }
                else
                {
                    cekilis.Gorsel = db.Cekilis.Find(cekilis.Id).Gorsel;
                }

                db.SaveChanges();
                return RedirectToAction("Index");
            }


            ViewBag.DersId = new SelectList(db.Ders, "Id", "Ad", cekilis.DersId);
            ViewBag.KonuId = new SelectList(db.Konu, "Id", "Ad", cekilis.KonuId);
            ViewBag.SinifId = new SelectList(db.Sinif, "Id", "Ad", cekilis.SinifId);
            return View(cekilis);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var cekilis = db.Cekilis.Find(id);
            if (cekilis == null) return HttpNotFound();
            return View(cekilis);
        }

        [HttpPost]
        [ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var cekilis = db.Cekilis.Find(id);
            db.Cekilis.Remove(cekilis);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing) db.Dispose();
            base.Dispose(disposing);
        }
    }
}