﻿using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LgsKocu.Web.Database;

namespace LgsKocu.Web.Areas.Admin.Controllers
{
    public class AdminSoruController : Controller
    {
        private readonly LgsHazirlikEntities db = new LgsHazirlikEntities();

        // GET: Admin/AdminSoru
        public ActionResult Index()
        {
            var soru = db.Soru.Include(s => s.Ders).Include(s => s.Sinif);
            return View(soru.ToList());
        }

        // GET: Admin/AdminSoru/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var soru = db.Soru.Find(id);
            if (soru == null) return HttpNotFound();
            return View(soru);
        }

        // GET: Admin/AdminSoru/Create
        public ActionResult Create()
        {
            ViewBag.UyeId = new SelectList(db.Uye, "Id", "Email");
            ViewBag.DersId = new SelectList(db.Ders, "Id", "Ad");
            ViewBag.SinifId = new SelectList(db.Sinif, "Id", "Ad");
            return View();
        }

        // POST: Admin/AdminSoru/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Soru soru, HttpPostedFileBase imageFile)
        {
            if (ModelState.IsValid)
            {
                if (imageFile != null)
                {
                    if (imageFile.ContentLength > 0)
                    {
                        var path = Path.Combine(Server.MapPath("~/Assets/Files/Soru/"), imageFile.FileName);
                        imageFile.SaveAs(path);

                        soru.Gorsel = "/Assets/Files/Soru/" + imageFile.FileName;
                    }
                }

                db.Soru.Add(soru);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UyeId = new SelectList(db.Uye, "Id", "Email");
            ViewBag.DersId = new SelectList(db.Ders, "Id", "Ad", soru.DersId);
            ViewBag.SinifId = new SelectList(db.Sinif, "Id", "Ad", soru.SinifId);
            return View(soru);
        }

        // GET: Admin/AdminSoru/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var soru = db.Soru.Find(id);
            if (soru == null) return HttpNotFound();
            ViewBag.UyeId = new SelectList(db.Uye, "Id", "Email");
            ViewBag.DersId = new SelectList(db.Ders, "Id", "Ad", soru.DersId);
            ViewBag.SinifId = new SelectList(db.Sinif, "Id", "Ad", soru.SinifId);
            return View(soru);
        }

        // POST: Admin/AdminSoru/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Soru soru, HttpPostedFileBase imageFile)
        {
            if (ModelState.IsValid)
            {
                if (imageFile != null)
                {
                    if (imageFile.ContentLength > 0)
                    {
                        var path = Path.Combine(Server.MapPath("~/Assets/Files/Soru/"), imageFile.FileName);
                        imageFile.SaveAs(path);

                        soru.Gorsel = "/Assets/Files/Soru/" + imageFile.FileName;
                    }
                }
                else
                {
                    soru.Gorsel = db.Soru.Find(soru.Id).Gorsel;
                }

                db.Entry(soru).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UyeId = new SelectList(db.Uye, "Id", "Email");
            ViewBag.DersId = new SelectList(db.Ders, "Id", "Ad", soru.DersId);
            ViewBag.SinifId = new SelectList(db.Sinif, "Id", "Ad", soru.SinifId);
            return View(soru);
        }

        // GET: Admin/AdminSoru/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var soru = db.Soru.Find(id);
            if (soru == null) return HttpNotFound();
            return View(soru);
        }

        // POST: Admin/AdminSoru/Delete/5
        [HttpPost]
        [ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var soru = db.Soru.Find(id);
            db.Soru.Remove(soru);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing) db.Dispose();
            base.Dispose(disposing);
        }
    }
}